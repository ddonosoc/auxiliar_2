# Auxiliar 2

---------

Para el desarrollo de esta auxiliar es necesario contar con las librerías glfw, OpenGL y numpy.

---------
En la modificación del shader, el contenido debe ser:

```outColor = vec4(
fragColor.x * 0.5,
fragColor.y * 0.5,
clamp(fragColor.z + 0.2, 0, 1) * 0.8,
1.0f);
```

---------
